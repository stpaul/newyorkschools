package com.tsp.learn.android.feature

import com.nhaarman.mockitokotlin2.*
import com.tsp.learn.android.feature.contract.SatContract
import com.tsp.learn.android.feature.data.SchoolSat
import com.tsp.learn.android.feature.repository.ISatRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SatPresenterTest {

    private val mockRepository = mock<ISatRepository>()
    private val mockView = mock<SatContract.View>()

    private lateinit var satPresenter: SatPresenter

    @Before
    fun setUp() {
        satPresenter = SatPresenter(mockRepository)
    }

    @Test
    fun testLoadSchools() {
        satPresenter = spy(satPresenter)
        satPresenter.attachView(mockView)
        doReturn(Observable.just(demandSchoolSat())).whenever(mockRepository).loadSatData(DBN)
        doReturn(Schedulers.trampoline()).whenever(satPresenter).io()
        doReturn(Observable.just(demandSchoolSat())).whenever(satPresenter)
            .bind(any<Observable<List<SchoolSat>>>())

        satPresenter.loadSchoolSatData(DBN)

        verify(mockView).onSchoolSatDataLoaded(demandSchoolSat())
        verify(mockRepository).loadSatData(DBN)
        verify(satPresenter).io()
        verify(satPresenter).ui()
        verify(satPresenter).bind(any<Observable<List<SchoolSat>>>())
    }

    @Test
    fun testLoadSchoolsWithError() {
        satPresenter = spy(satPresenter)
        satPresenter.attachView(mockView)
        doReturn(Observable.error<List<SchoolSat>>(Throwable("Failed!"))).whenever(mockRepository).loadSatData(DBN)
        doReturn(Observable.error<List<SchoolSat>>(Throwable("Failed!"))).whenever(satPresenter)
            .bind(any<Observable<List<SchoolSat>>>())
        doReturn(Schedulers.trampoline()).whenever(satPresenter).io()

        satPresenter.loadSchoolSatData(DBN)

        verify(mockView).showError()
        verify(mockRepository).loadSatData(DBN)
        verify(satPresenter).io()
        verify(satPresenter).ui()
    }

    private fun demandSchoolSat() = SchoolSat("New York Queens",
        123,
        234,
        344,
        234)

    private companion object {
        private const val DBN = "12AS4544"
    }

}