package com.tsp.learn.android.newyorkschools.di

import com.tsp.learn.android.newyorkschools.HomeActivity
import dagger.Module

@Module
internal interface AppModule {
    fun inject(homeActivity: HomeActivity)
}