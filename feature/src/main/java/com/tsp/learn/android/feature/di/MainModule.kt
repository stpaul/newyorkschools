package com.tsp.learn.android.feature.di

import com.tsp.learn.android.feature.SatActivity
import dagger.Module

@Module
internal interface MainModule {
    fun inject(satActivity: SatActivity)
}