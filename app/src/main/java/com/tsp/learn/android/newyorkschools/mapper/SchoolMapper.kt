package com.tsp.learn.android.newyorkschools.mapper

import com.tsp.learn.android.core.data.SchoolEntity
import com.tsp.learn.android.newyorkschools.data.School
import java.util.function.Function

object SchoolMapper : Function<List<SchoolEntity>, List<School>> {

    override fun apply(response: List<SchoolEntity>): List<School> =
        response.map { entry -> map(entry) }

    private fun map(schoolEntity: SchoolEntity): School =
        School(
            schoolEntity.dbn,
            schoolEntity.name,
            schoolEntity.phone,
            schoolEntity.city
        )
}