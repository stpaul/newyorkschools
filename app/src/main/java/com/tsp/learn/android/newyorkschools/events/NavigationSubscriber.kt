package com.tsp.learn.android.newyorkschools.events

interface NavigationSubscriber {
    fun navigateToSchoolSatPage(event: NavigateToSatEvent)
}
data class NavigateToSatEvent (val dbn: String)