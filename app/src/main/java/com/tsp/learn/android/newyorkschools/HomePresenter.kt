package com.tsp.learn.android.newyorkschools

import android.annotation.SuppressLint
import android.util.Log
import com.tsp.learn.android.core.mvp.RxBaseMvpPresenter
import com.tsp.learn.android.newyorkschools.contract.HomeContract
import com.tsp.learn.android.newyorkschools.events.NavigateToSatEvent
import com.tsp.learn.android.newyorkschools.events.NavigationSubscriber
import com.tsp.learn.android.newyorkschools.repository.SchoolRepository
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class HomePresenter @Inject constructor(private val repository: SchoolRepository,
                                        private val eventBus: EventBus): RxBaseMvpPresenter<HomeContract.View>(),
    HomeContract.Presenter, NavigationSubscriber {

    override fun attachView(view: HomeContract.View) {
        super.attachView(view)
        eventBus.register(this)
    }

    override fun detachView() {
        super.detachView()
        eventBus.unregister(this)
    }

    @SuppressLint("CheckResult")
    override fun loadSchools() {
        bind(repository.loadSchools())
            .subscribeOn(io())
            .observeOn(ui())
            .subscribe({ schoolList ->
                ifViewAttached { view ->
                    view.onSchoolLoaded(schoolList)
                }
            }, this::displayError)
    }

    private fun displayError(error: Throwable) {
        // TODO replace the logging with Timber library and also cover this method with a unit test
        Log.e(this.javaClass.simpleName,"Error loading saves data: ${error.message}")
        ifViewAttached { view ->
            view.showError()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun navigateToSchoolSatPage(event: NavigateToSatEvent) {
        ifViewAttached {view ->
            view.navigateToSchoolDetails(event.dbn)
        }
    }

    override fun io(): Scheduler = Schedulers.io()

    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
}