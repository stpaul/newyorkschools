package com.tsp.learn.android.newyorkschools.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.tsp.learn.android.newyorkschools.R
import com.tsp.learn.android.newyorkschools.data.School
import com.tsp.learn.android.newyorkschools.events.NavigateToSatEvent
import org.greenrobot.eventbus.EventBus

class SchoolViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @BindView(R.id.school_name_value)
    lateinit var schoolNameTv: TextView
    @BindView(R.id.school_city_value)
    lateinit var schoolCityTv: TextView
    @BindView(R.id.school_phone_value)
    lateinit var schoolPhoneTv: TextView
    @BindView(R.id.school_icon)
    lateinit var schoolIcon: TextView

    private var schoolDbn: String? = null
    private var eventBus: EventBus? = null

    init {
        ButterKnife.bind(this, itemView)
        itemView.setOnClickListener { navigateToNextPage() }
    }

    fun bind(school: School, eventBus: EventBus) {
        display(school)
        setSchoolIcon(school.name)
        this.eventBus = eventBus
    }

    private fun display(school: School) {
        schoolNameTv.text = school.name
        schoolCityTv.text = school.city
        schoolPhoneTv.text = school.phone
        schoolDbn = school.dbn
    }

    private fun setSchoolIcon(schoolName: String) {
        schoolIcon.text = schoolName.substring(0, 1).capitalize()
    }

    private fun navigateToNextPage() {
        schoolDbn?.let {
            eventBus?.post(NavigateToSatEvent(it))
        }
    }
}