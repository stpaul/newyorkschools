package com.tsp.learn.android.newyorkschools

import com.nhaarman.mockitokotlin2.*
import com.tsp.learn.android.newyorkschools.contract.HomeContract
import com.tsp.learn.android.newyorkschools.data.School
import com.tsp.learn.android.newyorkschools.events.NavigateToSatEvent
import com.tsp.learn.android.newyorkschools.repository.SchoolRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class HomePresenterTest {

    private val mockEventBus = mock<EventBus>()
    private val mockRepository = mock<SchoolRepository>()
    private val mockView = mock<HomeContract.View>()
    private val mockNavigateToSatEvent = mock<NavigateToSatEvent>()

    private lateinit var homePresenter: HomePresenter

    @Before
    fun setUp() {
        homePresenter = HomePresenter(mockRepository, mockEventBus)
    }

    @Test
    fun testLoadSchools() {
        homePresenter = spy(homePresenter)
        homePresenter.attachView(mockView)
        doReturn(Observable.just(demandSchools())).whenever(mockRepository).loadSchools()
        doReturn(Schedulers.trampoline()).whenever(homePresenter).io()
        doReturn(Observable.just(demandSchools())).whenever(homePresenter)
            .bind(any<Observable<List<School>>>())

        homePresenter.loadSchools()

        verify(mockView).onSchoolLoaded(demandSchools())
        verify(mockRepository).loadSchools()
        verify(homePresenter).io()
        verify(homePresenter).ui()
        verify(homePresenter).bind(any<Observable<List<School>>>())
    }

    @Test
    fun testLoadSchoolsWithError() {
        homePresenter = spy(homePresenter)
        homePresenter.attachView(mockView)
        doReturn(Observable.error<List<School>>(Throwable("Failed!"))).whenever(mockRepository)
            .loadSchools()
        doReturn(Observable.error<List<School>>(Throwable("Failed!"))).whenever(homePresenter)
            .bind(any<Observable<List<School>>>())
        doReturn(Schedulers.trampoline()).whenever(homePresenter).io()

        homePresenter.loadSchools()

        verify(mockView).showError()
        verify(mockRepository).loadSchools()
        verify(homePresenter).io()
        verify(homePresenter).ui()
    }

    @Test
    fun testNavigateToBookCheckout() {
        homePresenter.attachView(mockView)
        doReturn(DBN).whenever(mockNavigateToSatEvent).dbn

        homePresenter.navigateToSchoolSatPage(mockNavigateToSatEvent)

        verify(mockView).navigateToSchoolDetails(DBN)
        verify(mockNavigateToSatEvent).dbn
    }

    private fun demandSchools() = listOf(
        School(
            "12Z2313",
            "New york Queen",
            "234-455-4566",
            "New York"
        )
    )

    private companion object {
        private const val DBN = "123xds4"
    }
}