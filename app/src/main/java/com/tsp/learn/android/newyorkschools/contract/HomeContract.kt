package com.tsp.learn.android.newyorkschools.contract

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.tsp.learn.android.newyorkschools.data.School
import io.reactivex.Scheduler

interface HomeContract {

    interface View : MvpView {
        /**
         * @param schoolList the list of books to be displayed
         */
        fun onSchoolLoaded(schoolList: List<School>)

        /**
         * @param dbn will be used to download the school sat result
         *
         */
        fun navigateToSchoolDetails(dbn: String)

        /**
         * show error when an issue happens during the data loading
         */
        fun showError()
    }

    interface Presenter : MvpPresenter<View> {
        /**
         * load all new york school list
         */
        fun loadSchools()

        /**
         * @return rxjava io scheduler
         */
        fun io(): Scheduler

        /**
         * @return rxjava main scheduler
         */
        fun ui(): Scheduler
    }
}