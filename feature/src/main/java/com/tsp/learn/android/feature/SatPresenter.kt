package com.tsp.learn.android.feature

import android.annotation.SuppressLint
import android.util.Log
import com.tsp.learn.android.core.mvp.RxBaseMvpPresenter
import com.tsp.learn.android.feature.contract.SatContract
import com.tsp.learn.android.feature.repository.ISatRepository
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SatPresenter @Inject constructor(private val satRepository: ISatRepository):
    RxBaseMvpPresenter<SatContract.View>(), SatContract.Presenter {

    @SuppressLint("CheckResult")
    override fun loadSchoolSatData(dbn: String)  {
        bind(satRepository.loadSatData(dbn))
            .subscribeOn(io())
            .observeOn(ui())
            .subscribe({ schoolSat ->
                ifViewAttached { view ->
                    view.onSchoolSatDataLoaded(schoolSat)
                }
            }, this::displayError)
    }

    private fun displayError(error: Throwable) {
        // TODO replace the logging with Timber library and also cover this method with a unit test
        Log.e(this.javaClass.simpleName,"Error loading saves data: ${error.message}")
        ifViewAttached { view ->
            view.showError()
        }
    }

    override fun io(): Scheduler = Schedulers.io()

    override fun ui(): Scheduler = AndroidSchedulers.mainThread()

}