package com.tsp.learn.android.feature.contract

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.tsp.learn.android.feature.data.SchoolSat
import io.reactivex.Scheduler

interface SatContract {

    interface View : MvpView {
        /**
         * @param satDat the sat data to display
         */
        fun onSchoolSatDataLoaded(satDat: SchoolSat)

        /**
         * shows error when an issue happens during the data loading
         */
        fun showError()
    }

    interface Presenter : MvpPresenter<View> {
        /**
         * load the school sat data
         */
        fun loadSchoolSatData(dbn: String)

        /**
         * @return rxjava io scheduler
         */
        fun io(): Scheduler

        /**
         * @return rxjava main scheduler
         */
        fun ui(): Scheduler
    }
}