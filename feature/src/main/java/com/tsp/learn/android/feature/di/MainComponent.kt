package com.tsp.learn.android.feature.di

import com.tsp.learn.android.feature.SatActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MainModule::class, SatModule::class])
interface MainComponent {
    fun inject(mainActivity: SatActivity)
}