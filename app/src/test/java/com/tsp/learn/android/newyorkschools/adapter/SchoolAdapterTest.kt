package com.tsp.learn.android.newyorkschools.adapter

import android.view.ContextThemeWrapper
import android.widget.FrameLayout
import androidx.test.core.app.ApplicationProvider
import assertk.assertThat
import assertk.assertions.isInstanceOf
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.tsp.learn.android.newyorkschools.R
import com.tsp.learn.android.newyorkschools.adapter.viewholder.SchoolViewHolder
import com.tsp.learn.android.newyorkschools.data.School
import org.greenrobot.eventbus.EventBus
import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SchoolAdapterTest {

    private val mockEvent = mock<EventBus>()
    private val mockSchoolViewHolder = mock<SchoolViewHolder>()
    private val mockSchool = mock<School>()

    private lateinit var schoolAdapter: SchoolAdapter
    private lateinit var context: ContextThemeWrapper

    @Before
    fun setUp() {
        schoolAdapter = SchoolAdapter(mockEvent)
        context = ContextThemeWrapper(ApplicationProvider.getApplicationContext(), R.style.AppTheme)
    }

    @Test
    fun testOnCreateViewHolder() {
        assertThat(schoolAdapter.createViewHolder(FrameLayout(context), 0)).isInstanceOf(SchoolViewHolder::class.java)
    }

    @Test
    fun testOnBindViewHolder() {
        schoolAdapter.submitList(listOf(mockSchool))

        schoolAdapter.onBindViewHolder(mockSchoolViewHolder, 0)

        verify(mockSchoolViewHolder).bind(mockSchool, mockEvent)
    }
}