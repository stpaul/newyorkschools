package com.tsp.learn.android.newyorkschools.data

data class School(val dbn: String,
             val name: String,
             val phone: String,
             val city: String)