package com.tsp.learn.android.feature.mapper

import com.tsp.learn.android.core.data.SchoolSatEntity
import com.tsp.learn.android.feature.data.SchoolSat
import java.util.function.BiFunction

object SatMapper : BiFunction<List<SchoolSatEntity>, String, SchoolSat> {

    override fun apply(response: List<SchoolSatEntity>, filterToken: String): SchoolSat =
        map(response.first { entity -> filterToken.equals(entity.dbn, true)})

    private fun map(schoolSatEntity: SchoolSatEntity): SchoolSat =
        SchoolSat(
            schoolSatEntity.name,
            schoolSatEntity.numTakers.toInt(),
            schoolSatEntity.readingScore.toInt(),
            schoolSatEntity.mathScore.toInt(),
            schoolSatEntity.writingScore.toInt()
        )
}