package com.tsp.learn.android.feature.di.provider

import com.tsp.learn.android.feature.di.MainComponent

interface SatComponentProvider {
    fun getSatComponent(): MainComponent
}