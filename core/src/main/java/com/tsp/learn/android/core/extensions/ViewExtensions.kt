package com.tsp.learn.android.core.extensions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.inflateView(@LayoutRes layoutId: Int) =
    LayoutInflater.from(context)
        .inflate(layoutId, this, false)