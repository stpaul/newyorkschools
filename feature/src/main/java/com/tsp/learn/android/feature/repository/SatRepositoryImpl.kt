package com.tsp.learn.android.feature.repository

import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.feature.data.SchoolSat
import com.tsp.learn.android.feature.mapper.SatMapper
import io.reactivex.Observable
import javax.inject.Inject

class SatRepositoryImpl @Inject constructor(private val service: SchoolService) : ISatRepository {

    override fun loadSatData(dbn: String): Observable<SchoolSat> {
        return service.retrieveSchoolSatData()
            .flatMap { resp -> Observable.just(SatMapper.apply(resp, dbn)) }
    }
}