package com.tsp.learn.android.feature.repository

import com.tsp.learn.android.feature.data.SchoolSat
import io.reactivex.Observable

interface ISatRepository {
    /**
     * @param dbn the corresponding school dbn for filtering
     * the returned result
     * @return an observable of the school sat data
     */
    fun loadSatData(dbn: String): Observable<SchoolSat>
}