package com.tsp.learn.android.newyorkschools.repository

import com.tsp.learn.android.newyorkschools.data.School
import io.reactivex.Observable

interface SchoolRepository {
    /**
     * load all new york school
     */
    fun loadSchools() : Observable<List<School>>
}
