package com.tsp.learn.android.core.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class SchoolSatEntity(@JsonProperty("dbn") val dbn: String,
                      @JsonProperty("school_name") val name: String,
                      @JsonProperty("num_of_sat_test_takers") val numTakers: String,
                      @JsonProperty("sat_critical_reading_avg_score") val readingScore: String,
                      @JsonProperty("sat_math_avg_score") val mathScore: String,
                      @JsonProperty("sat_writing_avg_score") val writingScore: String)