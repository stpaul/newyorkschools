package com.tsp.learn.android.newyorkschools

import android.app.Application
import com.tsp.learn.android.core.module.RestModule
import com.tsp.learn.android.feature.di.DaggerMainComponent
import com.tsp.learn.android.feature.di.MainComponent
import com.tsp.learn.android.feature.di.SatModule
import com.tsp.learn.android.feature.di.provider.SatComponentProvider
import com.tsp.learn.android.newyorkschools.di.AppComponent
import com.tsp.learn.android.newyorkschools.di.DaggerAppComponent

class SchoolApplication : Application(), SatComponentProvider {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {
        appComponent = DaggerAppComponent.builder()
            .restModule(RestModule(this))
            .build()
    }

    override fun getSatComponent(): MainComponent =
        DaggerMainComponent.builder()
            .restModule(RestModule(this))
            .build()
}