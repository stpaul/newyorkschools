package com.tsp.learn.android.newyorkschools.di

import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.core.module.RestModule
import com.tsp.learn.android.newyorkschools.HomePresenter
import com.tsp.learn.android.newyorkschools.contract.HomeContract
import com.tsp.learn.android.newyorkschools.repository.SchoolRepository
import com.tsp.learn.android.newyorkschools.repository.SchoolRepositoryImpl
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module(includes = [RestModule::class])
class HomeModule {

    @Provides
    @Singleton
    fun provideHomePresenter(schoolRepository: SchoolRepository, eventBus: EventBus): HomeContract.Presenter =
        HomePresenter(schoolRepository, eventBus)

    @Provides
    @Singleton
    fun provideBookRepository(schoolService: SchoolService): SchoolRepository = SchoolRepositoryImpl(schoolService)
}