package com.tsp.learn.android.newyorkschools.repository

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.core.data.SchoolEntity
import com.tsp.learn.android.newyorkschools.data.School
import com.tsp.learn.android.newyorkschools.mapper.SchoolMapper
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.ArgumentMatchers

class SchoolRepositoryImplTest {

    private val mockService = mock<SchoolService>()
    private lateinit var schoolRepository: SchoolRepository

    @Before
    fun setUp() {
        schoolRepository = SchoolRepositoryImpl(mockService)
    }

    @Test
    fun testLoadSchools() {
        doReturn(Observable.just(demandListOfSchoolEntity())).whenever(mockService).retrieveNewYorkSchoolList()
        val testObserver = TestObserver<List<School>>()

        val observable = schoolRepository.loadSchools()
        observable.subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValue(SchoolMapper.apply(demandListOfSchoolEntity()))
    }

    private fun demandListOfSchoolEntity() = listOf(
        SchoolEntity(
            "12ER234",
            "New York Queen",
            "New York",
            "234-566-7790"
        )
    )
}