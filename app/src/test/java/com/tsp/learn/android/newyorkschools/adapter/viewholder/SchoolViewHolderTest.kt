package com.tsp.learn.android.newyorkschools.adapter.viewholder

import android.view.ContextThemeWrapper
import android.widget.FrameLayout
import androidx.test.core.app.ApplicationProvider
import assertk.assertThat
import assertk.assertions.hasToString
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.tsp.learn.android.core.extensions.inflateView
import com.tsp.learn.android.newyorkschools.R
import com.tsp.learn.android.newyorkschools.data.School
import com.tsp.learn.android.newyorkschools.events.NavigateToSatEvent
import org.greenrobot.eventbus.EventBus
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SchoolViewHolderTest {

    private val mockEventBus = mock<EventBus>()
    private lateinit var schoolViewHolder: SchoolViewHolder

    private lateinit var context: ContextThemeWrapper

    @Before
    fun setUp() {
        context = ContextThemeWrapper(ApplicationProvider.getApplicationContext(), R.style.AppTheme)
        schoolViewHolder = SchoolViewHolder(FrameLayout(context).inflateView(R.layout.school_item_layout))
    }

    @Test
    fun testBind() {
        schoolViewHolder.bind(demandSchool(), mockEventBus)

        assertThat(schoolViewHolder.schoolCityTv.text).hasToString(CITY)
        assertThat(schoolViewHolder.schoolPhoneTv.text).hasToString(PHONE)
        assertThat(schoolViewHolder.schoolNameTv.text).hasToString(NAME)
        assertThat(schoolViewHolder.schoolIcon.text).hasToString("N")

    }

    @Test
    fun testOnclickSendNavigateToSatEvent() {
        schoolViewHolder.bind(demandSchool(), mockEventBus)

        schoolViewHolder.itemView.performClick()

        verify(mockEventBus).post(NavigateToSatEvent(DBN))
    }

    private fun demandSchool() = School(DBN, NAME, PHONE, CITY)

    private companion object {
        private const val DBN = "123xds4"
        private const val NAME = "New york Queen"
        private const val PHONE = "234-455-4566"
        private const val CITY = "New York"
    }
}