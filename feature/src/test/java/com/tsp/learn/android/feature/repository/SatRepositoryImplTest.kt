package com.tsp.learn.android.feature.repository

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.core.data.SchoolEntity
import com.tsp.learn.android.core.data.SchoolSatEntity
import com.tsp.learn.android.feature.data.SchoolSat
import com.tsp.learn.android.feature.mapper.SatMapper
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class SatRepositoryImplTest {

    private val mockService = mock<SchoolService>()
    private lateinit var satRepositoryImpl: SatRepositoryImpl
    @Before
    fun setUp() {
        satRepositoryImpl = SatRepositoryImpl(mockService)
    }

    @Test
    fun loadSatData() {
        doReturn(Observable.just(demandListOfSchoolSatEntity())).whenever(mockService).retrieveSchoolSatData()
        val testObserver = TestObserver<SchoolSat>()

        val observable = satRepositoryImpl.loadSatData(DBN)
        observable.subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValue(SatMapper.apply(demandListOfSchoolSatEntity(), DBN))
    }

    private fun demandListOfSchoolSatEntity() = listOf(
        SchoolSatEntity(
            DBN,
            "New York Queen",
            "233",
            "432",
            "565",
            "322"
        )
    )

    private companion object {
        private const val DBN = "123XBW"
    }
}