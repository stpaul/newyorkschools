package com.tsp.learn.android.newyorkschools.di

import com.tsp.learn.android.newyorkschools.HomeActivity
import com.tsp.learn.android.newyorkschools.SchoolApplication
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, HomeModule::class])
interface AppComponent {

    fun inject(application: SchoolApplication)

    fun inject(mainActivity: HomeActivity)
}