package com.tsp.learn.android.feature.di

import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.core.module.RestModule
import com.tsp.learn.android.feature.SatPresenter
import com.tsp.learn.android.feature.contract.SatContract
import com.tsp.learn.android.feature.repository.ISatRepository
import com.tsp.learn.android.feature.repository.SatRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RestModule::class])
class SatModule {

    @Provides
    @Singleton
    fun provideSatPresenter(satRepository: ISatRepository): SatContract.Presenter = SatPresenter(satRepository)

    @Provides
    @Singleton
    fun provideSatRepository(schoolService: SchoolService): ISatRepository = SatRepositoryImpl(schoolService)

}