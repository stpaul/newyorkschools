package com.tsp.learn.android.newyorkschools.adapter.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.tsp.learn.android.newyorkschools.data.School
import java.util.*

class SchoolDiffUtil: DiffUtil.ItemCallback<School>() {

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean =
        Objects.equals(oldItem, newItem)

    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean =
        Objects.equals(oldItem.dbn, newItem.dbn)
}