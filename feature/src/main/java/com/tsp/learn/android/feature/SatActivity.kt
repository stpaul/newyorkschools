package com.tsp.learn.android.feature

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.tsp.learn.android.feature.contract.SatContract
import com.tsp.learn.android.feature.data.SchoolSat
import com.tsp.learn.android.feature.di.provider.SatComponentProvider
import kotlinx.android.synthetic.main.sat_activity_main.*
import javax.inject.Inject

// TODO add unit test cover this activity
class SatActivity : MvpActivity<SatContract.View, SatContract.Presenter>(), SatContract.View {

    @Inject
    lateinit var satPresenter: SatContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sat_activity_main)

        val dbn = intent.getStringExtra(DBN_KEY)
        dbn?.let {
           satPresenter.loadSchoolSatData(it)
        }
    }

    override fun onSchoolSatDataLoaded(satDat: SchoolSat) {
        school_name.text = satDat.name
        school_sat_taker_value.text = satDat.numOfTakers.toString()
        school_sat_read_value.text = satDat.readingScore.toString()
        school_sat_write_value.text = satDat.writingScore.toString()
        school_sat_math_value.text = satDat.mathScore.toString()
    }

    override fun createPresenter(): SatContract.Presenter {
        // FIXME this is a hack. The object graph initialization should be inside onCreate or inside the init() method
        inject()
        return satPresenter
    }

    override fun showError() {
        Toast.makeText(this, R.string.error_msg, Toast.LENGTH_LONG).show()
    }

    private fun inject() {
        (application as SatComponentProvider).getSatComponent().inject(this)
    }

    private companion object {
        private const val DBN_KEY = "dbn"
    }
}
