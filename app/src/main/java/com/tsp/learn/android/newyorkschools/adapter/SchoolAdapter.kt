package com.tsp.learn.android.newyorkschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.tsp.learn.android.newyorkschools.R
import com.tsp.learn.android.newyorkschools.adapter.diffutils.SchoolDiffUtil
import com.tsp.learn.android.newyorkschools.adapter.viewholder.SchoolViewHolder
import com.tsp.learn.android.newyorkschools.data.School
import org.greenrobot.eventbus.EventBus

class SchoolAdapter(private val eventBus: EventBus): ListAdapter<School, SchoolViewHolder>(SchoolDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder =
        SchoolViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.school_item_layout, parent, false))

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(getItem(position), eventBus)
    }
}