package com.tsp.learn.android.feature.data

data class SchoolSat(val name: String,
                val numOfTakers: Int,
                val readingScore: Int,
                val mathScore: Int,
                val writingScore: Int)