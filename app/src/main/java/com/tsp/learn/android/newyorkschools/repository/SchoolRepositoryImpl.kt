package com.tsp.learn.android.newyorkschools.repository

import com.tsp.learn.android.core.SchoolService
import com.tsp.learn.android.core.utils.Constants
import com.tsp.learn.android.newyorkschools.data.School
import com.tsp.learn.android.newyorkschools.mapper.SchoolMapper
import io.reactivex.Observable
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(private val schoolService: SchoolService): SchoolRepository {

    override fun loadSchools(): Observable<List<School>> =
        schoolService.retrieveNewYorkSchoolList()
            .flatMap { response -> Observable.just(SchoolMapper.apply(response)) }
}