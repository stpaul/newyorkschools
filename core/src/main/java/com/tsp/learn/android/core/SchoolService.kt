package com.tsp.learn.android.core

import com.tsp.learn.android.core.data.SchoolEntity
import com.tsp.learn.android.core.data.SchoolSatEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers

interface SchoolService {
    // TODO add a request interceptor to add the token. In fact add the token to the gradle file
    // TODO and use the buildconfig to read the token value and pass it to a request interceptor
    @Headers("X-App-Token: GyAAAsvdruuseFLQ9hG902sLI")
    @GET("resource/s3k6-pzi2.json")
    fun retrieveNewYorkSchoolList(): Observable<List<SchoolEntity>>

    // TODO same as above
    @Headers("X-App-Token: GyAAAsvdruuseFLQ9hG902sLI")
    @GET("resource/f9bf-2cp4.json")
    fun retrieveSchoolSatData(): Observable<List<SchoolSatEntity>>
}