package com.tsp.learn.android.newyorkschools

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.tsp.learn.android.feature.SatActivity
import com.tsp.learn.android.newyorkschools.adapter.SchoolAdapter
import com.tsp.learn.android.newyorkschools.contract.HomeContract
import com.tsp.learn.android.newyorkschools.data.School
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

// TODO add unit test to cover this class
class HomeActivity : MvpActivity<HomeContract.View, HomeContract.Presenter>(), HomeContract.View {

   @BindView(R.id.school_list)
   lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var homePresenter: HomeContract.Presenter

    @Inject
    lateinit var eventBus: EventBus

    @VisibleForTesting
    var schoolAdapter: SchoolAdapter? = null

    init {
        SchoolApplication.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        schoolAdapter = SchoolAdapter(eventBus)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
        }

        recyclerView.adapter = schoolAdapter

        homePresenter.loadSchools()
    }

    override fun onSchoolLoaded(schoolList: List<School>) {
        schoolAdapter?.submitList(schoolList)
    }

    override fun navigateToSchoolDetails(dbn: String) {
        // TODO replace this by a deeplink manager class
        val intent = Intent(this, SatActivity::class.java)
        intent.putExtra(DBN_KEY, dbn)
        startActivity(intent)
    }

    override fun showError() {
        Toast.makeText(this, R.string.error_msg, Toast.LENGTH_SHORT).show()
    }

    override fun createPresenter(): HomeContract.Presenter = homePresenter

    private companion object {
        private const val DBN_KEY = "dbn"
    }
}
