package com.tsp.learn.android.core.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class SchoolEntity(@JsonProperty("dbn") val dbn: String,
                   @JsonProperty("school_name") val name: String,
                   @JsonProperty("city") val city: String,
                   @JsonProperty("phone_number") val phone: String)